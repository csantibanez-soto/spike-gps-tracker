// RGB Pin Definition
const int R_LED = 25;
const int G_LED = 26;
const int B_LED = 27;

// Serial2 Pin Definition
const int RXD2 = 16;
const int TXD2 = 17;

// The TinyGPS++ object
TinyGPSPlus gps;

// MPU9250 object
Adafruit_MPU6050 mpu;

// Variables definition
String gps_latitude;
String gps_longitude;

String _month;
String _day;
String _year;

String _hour;
String _minute;
String _second;
String _centisecond;

TaskHandle_t Task1;

String batt;

String magX;
String magY;
String magZ;

String gyrX;
String gyrY;
String gyrZ;

String accX;
String accY;
String accZ;

String Batt = "0";
