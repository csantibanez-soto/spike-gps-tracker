// Write file to SD Card
void writeFile(fs::FS &fs, const char * path, const char * message){
//    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
//        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
//        Serial.println("File written");
    } else {
//        Serial.println("Write failed");
    }
    file.close();
}

//Append data to a File
void appendFile(fs::FS &fs, const char * path, const char * message){
//    Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
//        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
//        Serial.println("Message appended");
    } else {
//        Serial.println("Append failed");
    }
    file.close();
}

// Guardar datos obtenidos en la memoria SD
void storeData(String d1, String d2, String d3, String d4, String d5, String d6, 
              String d7, String d8, String d9, String d10, String d11, String d12, 
              String d13, String d14, String d15, String d16){
  
  String _data;
  _data = d1 + "," + d2 + "," + d3 + "," + d4 + "," + d5 + "," + d6 + "," + d7 + "," + d8 + "," + d9 + "," + d10 + "," + d11 + "," + d12 + "," + d13 + "," + d14 + "," + d15 + "," + d16 + "\n" ;
  Serial.println("Year,Month,Day,Hour,Minute,Second,Centisecond,Latitude,Longitude,GyrX,GyrY,GyrZ,AccX,AccY,AccZ,Batt");
  Serial.println(_data);
  appendFile(SD, "/data.csv", _data.c_str());
}
