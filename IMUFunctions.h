void imuData(){
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);
  
  accX = String(a.acceleration.x, 6);
  accY = String(a.acceleration.y, 6);
  accZ = String(a.acceleration.z, 6);

  gyrX = String(g.gyro.x, 6);
  gyrY = String(g.gyro.y, 6);
  gyrZ = String(g.gyro.z, 6);

//  magX = String(mpu.getMagX(), 6);
//  magY = String(mpu.getMagY(), 6);
//  magZ = String(mpu.getMagZ(), 6);
}
