#include "Libraries.h"

void setup(){
  // Se inicia el Serial para debugging y el Serial2 para comunicación con el GPS
  Serial.begin(115200);
  delay(1000);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  delay(1000);

  // Se inicia el IMU
  Wire.begin();
  mpu.begin(0x69);
  delay(1000);

  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  mpu.setGyroRange(MPU6050_RANGE_2000_DEG);
  mpu.setFilterBandwidth(MPU6050_BAND_260_HZ);



  xTaskCreatePinnedToCore(
      gpsData, /* Function to implement the task */
      "Task1", /* Name of the task */
      10000,  /* Stack size in words */
      NULL,  /* Task input parameter */
      0,  /* Priority of the task */
      &Task1,  /* Task handle. */
      0); /* Core where the task should run */

  // Se inicia la tarjeta SD y se escribe la primera fila con el nombre de las columnas de datos
  SD.begin(5);
  delay(1000);
  writeFile(SD, "/data.csv", "Year,Month,Day,Hour,Minute,Second,Centisecond,Latitude,Longitude,GyrX,GyrY,GyrZ,AccX,AccY,AccZ,Batt\n");
  
}

void loop(){
  delay(100);
  imuData();
  storeData(_year, _month, _day, _hour, _minute, _second, _centisecond, gps_latitude, gps_longitude, gyrX, gyrY, gyrZ, accX, accY, accZ, Batt);
}
